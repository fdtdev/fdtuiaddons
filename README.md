# FDT UI Addons

UI Addons


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.uiaddons": "https://bitbucket.org/fdtdev/fdtuiaddons.git#2.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtuiaddons/src/2.0.0/LICENSE.md)