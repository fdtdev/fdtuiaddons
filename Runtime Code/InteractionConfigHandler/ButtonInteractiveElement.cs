﻿using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.UIAddons.InteractionConfigHandler
{
    public class ButtonInteractiveElement : InteractiveElementBase
    {
        [SerializeField] protected UnityEvent _onSetInvisibleEvt;
        [SerializeField] protected UnityEvent _onSetInteractableEvt;
        [SerializeField] protected UnityEvent _onSetNotInteractableEvt;

        protected override void SetInvisible()
        {
            _onSetInvisibleEvt.Invoke();
        }

        protected override void SetInteractable()
        {
            _onSetInteractableEvt.Invoke();
        }

        protected override void SetNotInteractable()
        {
            _onSetNotInteractableEvt.Invoke();
        }
    }
}