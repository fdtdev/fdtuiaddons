﻿using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.UIAddons.InteractionConfigHandler
{
    [CreateAssetMenu(menuName = "FDT/InteractionConfigHandler/InteractiveGroupAsset")]
    public class InteractiveGroupAsset : InteractiveElementAsset
    {
        [SerializeField] protected List<InteractiveElementData> _elements = new List<InteractiveElementData>();
        public List<InteractiveElementData> Elements
        {
            get { return _elements; }
        }
    }
}