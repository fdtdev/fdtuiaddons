﻿#if SMGRAPH
using System;
using com.fdt.genericdatasaving;
using com.fdt.statemachinegraph;
using UnityEngine;

namespace com.FDT.UIAddons.InteractionConfigHandler
{
    [CreateAssetMenu(menuName = "FDT/InteractionConfigHandler/InteractiveGroupStateSetting")]

    public class InteractiveGroupStateSetting : StateSettingAsset
    {
        struct Param1 : IContentParameter
        {
            public VarType GetVarType
            {
                get { return VarType.ASSET; }
            }

            public Type GetAssetType
            {
                get { return typeof(InteractiveGroupAsset); }
            }

            public bool GetTypeFromAsset { get; }
            public string GetCustomDescription { get; }
        }

        protected IContentParameter[] _params = new IContentParameter[] {new Param1()};

        public override IContentParameter[] Parameters
        {
            get { return _params; }
        }
        public override void EnterExecutionRange(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] _valueDatas,
            TimeRangeData timeRange, float time, float nTime)
        {
            base.EnterExecutionRange(contentIdx, smNode, cdata, _valueDatas, timeRange, time, nTime);
            
            InteractiveGroupAsset item = _valueDatas[0].AssetValue as InteractiveGroupAsset;
            if (item != null)
            {
                for (int i = 0; i < item.Elements.Count; i++)
                {
                    var e = item.Elements[i].elementId;
                    var s = item.Elements[i].State;
                    if (e == null)
                    {
                        Debug.LogError($"elementID not found.");
                    }
                    InteractiveElementsHandler.Instance.SetState(e, s);
                }
            }
        }
    }
}
#endif