﻿#if SMGRAPH
using System;
using com.fdt.genericdatasaving;
using com.fdt.statemachinegraph;
using UnityEngine;


namespace com.FDT.UIAddons.InteractionConfigHandler
{
    [CreateAssetMenu(menuName = "FDT/InteractionConfigHandler/InteractiveElementStateSetting")]
    public class InteractiveElementStateSetting : StateSettingAsset
    {
        struct Param1 : IContentParameter
        {
            public VarType GetVarType
            {
                get { return VarType.ASSET; }
            }

            public Type GetAssetType
            {
                get { return typeof(InteractiveElementAsset); }
            }

            public bool GetTypeFromAsset { get; }
            public string GetCustomDescription { get; }
        }
        struct Param2 : IContentParameter
        {
            public VarType GetVarType
            {
                get { return VarType.INT; }
            }

            public Type GetAssetType
            {
                get { return typeof(InteractiveElementState); }
            }

            public bool GetTypeFromAsset { get; }
            public string GetCustomDescription { get; }
        }
        protected IContentParameter[] _params = new IContentParameter[] {new Param1(), new Param2()};

        public override IContentParameter[] Parameters
        {
            get { return _params; }
        }
        public override void EnterExecutionRange(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] _valueDatas,
            TimeRangeData timeRange, float time, float nTime)
        {
            base.EnterExecutionRange(contentIdx, smNode, cdata, _valueDatas, timeRange, time, nTime);
            InteractiveElementAsset item = _valueDatas[0].AssetValue as InteractiveElementAsset;
            if (item == null)
            {
                Debug.LogError($"elementID not found.");
            }
            else
            {
                InteractiveElementState s = (InteractiveElementState)_valueDatas[1].IntValue;
                InteractiveElementsHandler.Instance.SetState(item, s);
            }
        }
    }
}
#endif