﻿namespace com.FDT.UIAddons.InteractionConfigHandler
{
    public enum InteractiveElementState
    {
        INTERACTABLE,
        NO_INTERACTABLE,
        INVISIBLE
    }
}