﻿using UnityEngine;

namespace com.FDT.UIAddons.InteractionConfigHandler
{
    [System.Serializable]
    public class InteractiveElementData
    {
        [SerializeField] protected InteractiveElementAsset _elementID;
        [SerializeField] protected InteractiveElementState _state;

        public InteractiveElementAsset elementId => _elementID;
        public InteractiveElementState State => _state;

        public void SetState()
        {
            InteractiveElementBase item = InteractiveElementsHandler.Instance.GetInstance<InteractiveElementBase>(_elementID);
            if (item != null)
            {
                item.SetState(_state);
            }
        }
    }
}