﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.UIAddons.InteractionConfigHandler
{
    public abstract class InteractiveElementBase : MonoBehaviour, IRegistrable<InteractiveElementAsset>
    {
        [SerializeField] protected InteractiveElementAsset _referenceID;
        [SerializeField] protected InteractiveElementState _state;
        protected void OnEnable()
        {
            InteractiveElementsHandler.Instance.Register(this, ReactToRegistration);
            HandleOnEnable();
            ChangeState();
        }

        protected virtual void HandleOnEnable()
        {
        }

        protected void OnDisable()
        {
            HandleOnDisable();
            InteractiveElementsHandler.Instance.Unregister(this, ReactToUnregistration);
        }

        protected virtual void HandleOnDisable()
        {
        }

        protected virtual void ReactToRegistration()
        {

        }

        protected virtual void ReactToUnregistration()
        {

        }

        public InteractiveElementAsset referenceID
        {
            get { return _referenceID; }
        }

        public void SetState(InteractiveElementState state)
        {
            if (state != _state)
            {
                _state = state;
                ChangeState();
            }
        }
        protected void ChangeState()
        {
            switch (_state)
            {
                case InteractiveElementState.INVISIBLE:
                    SetInvisible();
                    break;
                case InteractiveElementState.INTERACTABLE:
                    SetInteractable();
                    break;
                case InteractiveElementState.NO_INTERACTABLE:
                    SetNotInteractable();
                    break;
            }
        }
        protected abstract void SetInvisible();
        protected abstract void SetInteractable();
        protected abstract void SetNotInteractable();
    }
}