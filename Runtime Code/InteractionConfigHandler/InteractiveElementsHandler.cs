﻿using com.FDT.Common;

namespace com.FDT.UIAddons.InteractionConfigHandler
{
    public class InteractiveElementsHandler : SingletonRegisterer<InteractiveElementsHandler, InteractiveElementBase, InteractiveElementAsset>
    {
        public void SetState(InteractiveElementAsset item, InteractiveElementState state)
        {
            var obj = GetInstance<InteractiveElementBase>(item);
            SetState(obj, state);
        }
        public void SetState(InteractiveElementBase item, InteractiveElementState state)
        {
            item.SetState(state);
        }
    }
}