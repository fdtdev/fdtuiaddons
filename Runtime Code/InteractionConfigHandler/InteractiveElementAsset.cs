﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.UIAddons.InteractionConfigHandler
{
    [CreateAssetMenu(menuName = "FDT/InteractionConfigHandler/InteractiveElementAsset")]
    public class InteractiveElementAsset : ResetScriptableObject, IRegistrableID
    {
    }
}